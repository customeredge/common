{
	"cluster_name":"k8s-operations",
	"parameters" : {
		"cluster": {
			"client" : {
				"env_prefix": "di",
				"pf_zone":"edge",
				"cluster_type":"k8s-fe-rke",
				"cluster_fqdn":"k8s-api.edge.inter-cdnrd.orange-business.com",
				"k8s_version":"v1.22.17-rancher1-2",
				"docker_version":"20.10.5",
				"count_worker":"3",
				"count_worker_gpu":"0",
				"flavor_name_worker":"s3.2xlarge.2",
				"flavor_name_worker_gpu":"p2.2xlarge.8",
				"use_harbor":"true",
				"use_k8boards":"false",
				"additional_storage":"",
				"snapshot_to_restore":""
			},
			"admin" : {
				"project_stream":"k8s",
				"domain":"edge.inter-cdnrd.orange-business.com",
				"backend_tf":"s3",
				"prefix_backend_path":"edge-fe/edge-k8s",
				"image_name":"OBS Ubuntu 18.04",
				"ssh_user": "cloud",
				"count_master_etcd":"3",
				"count_etcd":"0",
				"count_master":"0",
				"flavor_name_master_etcd":"s3.large.4",
				"flavor_name_master":"s3.large.2",
				"flavor_name_etcd":"s3.large.4",
				"k8s_backup_interval":"6",
				"k8s_backup_retention":"14",
				"https_proxy": "",
				"http_proxy": "",
				"no_proxy": "",
				"registry_mirror":"",
				"docker_volume_size_gb": "50",
				"etcd_volume_size_gb": "5",
				"cni":"canal",
				"cluster_domain": "cluster.local",
				"cluster_cidr": "10.42.0.0/16",
				"service_cidr": "10.43.0.0/16",
				"cluster_dns_server": "10.43.0.10",
				"enable_logging":"false",
				"enable_cluster_monitoring":"false",
				"use_consul":"false",
				"use_proxy_runners":"false",
				"enable_etcd_snapshots":"true",
				"create_loadbalancer": "true",
				"use_remote_state":"false",
				"use_env_prefix_dns":"false"
			}
		},
		"infrastructure" : {
			"cloud" : {
				"OS_DOMAIN_NAME": "OCB0004965",
				"OS_AUTH_URL": "https://iam.eu-west-0.prod-cloud-ocb.orange-business.com/v3",
				"OS_REGION_NAME": "eu-west-0",
				"OS_TENANT_ID": "57d8ad1d36c4446a9ed4a56ffde1c84b",
				"OS_USERNAME": "loam7356",
				"OS_PASSWORD": "cust0mer&dge",
				"OS_PRIVATE_KEY": "-----BEGIN RSA PRIVATE KEY-----\nMIIEoQIBAAKCAQEAtDWSzhNtVgJL5Mj0nqIlo+OhCcxaQdlo4eb0EoaR8uB35HGG\nBqLjDFxUcFJ6yooZbYCvzuBeA//zTSHUTJd0J7hfITyigvta7Jgz3+Jh1wD9aZT/\neR2i8YYac3G/6OMwsGfq7mOIOG87bmRaHjI2GYvVgevdJbbU2masaloIzSNKA6CS\nGMAXcxFg8qDA50Y/xg9YDg309W32F5DtBBzdNdufhYeX3iM4K1iMXtOUttJBhqiI\nqBSdlT+53lJrmNxFApP7LJRD8P56DZANYMZqIUXqu1cduMw4Q1oGNTKROMfTDL+g\nxqg/rtiox1109824I2GtsDkIZ/wAg2loCaV1nwIBJQKCAQA/URfnikkCiy9sD0Ew\n0S/RzISwamTmtCvE/h5nX7a9Hm9QQ4/0fmtzC673CDj7De1CJkubtp2Ss9/c1Ijc\no+qKfwyq0B1sSnnkbNP0t1LPAFkJa7O07ry1uX7qSo+JLTqe2GdM1tzVjt2Hprfh\nGI+Mb2axG4UNQDzyyiDnGLb79wt/oZuEzugb6PK3Ukw6HAzpt2kWRBax1mp0sy5V\nsmG0RxouZmqv/TzJVH1bnIzls8vEumdN5kSk0vvBcGhP4lV4wXM4CfLPQ0OYUNuA\ny1Fi4EUXvM53vECz3J8jQaRWEUy6/cHRrEtuFkzhqtKhTMT4dfx1q5/oeY+/4QXt\nMaK1AoGBAPw7rGuK0dDMdhiL07H2ojdhgmw1w5BJ0aRdn6wopPy3/vhp1B03/bBh\nST1gee/oZj4cq+Au0n/jtJCPTBZ4e1w7kPFVARLymY0HX4SXn2m3YJmrCUckY86u\nAItXSHAS+b01DSO63yD1llF1gBzDw5Wn2vrLWOixFsKGSLhoppyTAoGBALbmiqcA\nPu0cF/gORUmttqiQR9RxiJo7TcBGaW42VMppEkjmaJnE8lDvgUyq1yd7uw46EYJ0\nKOQrMquK7cnG3LpvUpqG2VaJG6LK1D9uRsoBtcd9+WOOZE735Y6NQv6m/08l8/Ly\nGdHiZVUeWZ5BkhlCNuRa4/YhPH14jIsRmPZFAoGAFHOKhUKbY/Tm+xJBmM7O4eVP\nw5WoEp4zl7SQZ+eerLT4/2KG0e/IdhW51ItIKDVwEt+66KnZt1em20njkx6GivAS\nq8ifOOM84evXT/CeOQEH1Ru0pOdGXN2tBGEF35LV+pWZSBYSF23bwWpkVVv7Gfja\nZ1yYgZHRabfcYfqluasCgYAni8rzrQavzrkTCgEkr/AkcjkLVtFtdJswfe1IQxlA\nhWu8vDJKwsyb4MICvSeZ1ZA6bWuR0+0qajR4HgnmcC+5m4d7W21DJJBajLhFEOy2\nDjUkNunQViOGsiPLy4QbYmC3Ksz2Qi8Yn6c0/6SsmI5KpohoubS/mHvIGhCMxYjb\nTQKBgQCtjoG4SsHv2ZNDOB9zArAtRamI2TJJzSsgf4oBJYJcyb+F+39Fi6p0dQqd\nQ7+ZK0Il7OQKFVCMSGDUj2815pcDamiIA6PcB6C/8KhckgKk8NJN8dmucblkuVBs\nlJg7KkyPw09Xxgkbi+j8t3xLnSGfBEjVOAUOauTXHx6yydu89A==\n-----END RSA PRIVATE KEY-----",
				"CLUSTER_CA": "-----BEGIN CERTIFICATE-----\nMIIFeTCCA2GgAwIBAgISESFBPLDiSDHsUF/6OuQhW5HXMA0GCSqGSIb3DQEBCwUA\nMF8xCzAJBgNVBAYTAkZSMQ8wDQYDVQQKDAZPcmFuZ2UxGDAWBgNVBAsMD0ZSIDg5\nIDM4MDEyOTg2NjElMCMGA1UEAwwcT3JhbmdlIEludGVybmFsIEcyIFNlcnZlciBD\nQTAeFw0yMDA1MDcwOTA3NDJaFw0yMjA1MDcwOTA3NDJaMCUxIzAhBgNVBAMTGnJh\nbmNoZXIuZGktazhzLnRlY2gub3JhbmdlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A\nMIIBCgKCAQEAx0m5gbLyrqWHp50p7AEs/N2HHqdcT1+90jjXtyk2pc0FDzgfAYVo\ntb7dZAoftJcreCKL3B5+AJFe76EgOGDefKHPBaACyZIo/QFhuit2+O61XAaIgFbt\npdwmKWCfVo5chLw4GUi82ukKLfr/7GVPUmQlO3LbBvIYAymydpjZRKrsrFGeFDwa\nNUeNMyInoAfhLgbrI7LJO49+LMQ3wTAe+2u+VBvLT6mp36Pb6YKp2jtl3jmlTPmG\nmGF7kAGKc5fbloRD0tt5WVeslal6Qy/PWcVVNwXvquJCgPFSOb/J09r/RZBPQ3Df\nJloZebbzU3SI5N/OPKHMmjt6EZvBE3c+7QIDAQABo4IBZzCCAWMwDgYDVR0PAQH/\nBAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAlBgNVHREEHjAc\nghpyYW5jaGVyLmRpLWs4cy50ZWNoLm9yYW5nZTA/BgNVHSAEODA2MDQGCSqBegEQ\nDAoCATAnMCUGCCsGAQUFBwIBFhlodHRwOi8vcGtpLm9yYW5nZS5jb20vY3BzMIGJ\nBgNVHR8EgYEwfzBAoD6gPIY6aHR0cDovL3BraS1jcmwuaXRuLmZ0Z3JvdXAvY3Js\nL29yYW5nZWludGVybmFsZzItc2VydmVyLmNybDA7oDmgN4Y1aHR0cDovL3BraS5v\ncmFuZ2UuY29tL2NybC9vcmFuZ2VpbnRlcm5hbGcyLXNlcnZlci5jcmwwHQYDVR0O\nBBYEFNR2CkUuUCLW+J1GMgj1vjAelJxxMB8GA1UdIwQYMBaAFLw95KTHvIsxC1Xi\nwweiovjb17twMA0GCSqGSIb3DQEBCwUAA4ICAQB9xayoeI2FjrMADSzuf3SnWr6+\nwapR6U7PWRfhYAkQ6UumnqPV+LQdt4VdYSvCcESU7sRc/5MbwadAx7wydRrTz7oH\nTXKP3rXAUZnTYTDpsxAYwHCrfn7GXCCUazzHrepCdCACwKp311WRy5nVX7KHWkNt\nSmnCxBqJmLhPHGwx5svq2Pr1UL1HphNPe0fhTEHn1Al9pD6GV5JSHw5SPfN9Iked\nD5cxTCaILbAAYwpoAWHNbULLYcPp2+KclYQ/481kNcKRREXqBV2GDXY/hTVwAAI6\nKlBx3fjlDGXy5SNLVN7fyGraLRHG+/KapMCtxPfGaYtJD4X5jBiZBwWE6AcI+l6I\ng/9dobWuai41icSbr6Ai9nnOgMUlUxt1fpPn8i3D7iqSFZqES/ZGOTCjD3zSiEAm\nwHnFoRkQpHLWPTjjssz1J99ELthn4lO+me/We+lJecks7gQ0Plg2H77AN/iISTzB\nWsOYNpZyuDeBxNdZCoIzJlKdTKzWGQjSZh4zvEe0ZJjNkVEvP/gwCxARAr8gopxR\nhXos7bdUcC5EF1ZCej42Z7N1Sw8mBYR4iFd7e5MNWfXW/V2P7VIduR4sv8+yX0+7\n6Gior5TUCUJ6dtrFGkj0c6M5l449IZJaTJn/2an/jVR9TjxcFA9AfP5YpJMt74/f\ntpq/u/nzW2FV5lomQw==\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIGPTCCBCWgAwIBAgIBAjANBgkqhkiG9w0BAQsFADBdMQswCQYDVQQGEwJGUjEP\nMA0GA1UECgwGT3JhbmdlMRgwFgYDVQQLDA9GUiA4OSAzODAxMjk4NjYxIzAhBgNV\nBAMMGk9yYW5nZSBJbnRlcm5hbCBHMiBSb290IENBMB4XDTE2MDUwMzEzMjA0NFoX\nDTM2MDUwMzEzMjA0NFowXzELMAkGA1UEBhMCRlIxDzANBgNVBAoMBk9yYW5nZTEY\nMBYGA1UECwwPRlIgODkgMzgwMTI5ODY2MSUwIwYDVQQDDBxPcmFuZ2UgSW50ZXJu\nYWwgRzIgU2VydmVyIENBMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA\nmPYx/p4jDiGaWI25AolfgwC2p/fI209Jjx++FfKnFb9OfFC66Wuny2ziA+2uuPXW\nOI118DGfhfS45dyf/BYo2v70kMiIvZI1FN5pcaCDpYSTxGmWxcaiVm8BIzFIT7HQ\ngK61jYeJrsTrzoUYAgZVnF+pZZgb04Sy+SdVctNWVKKuXfx59XiXqpq2WSwULb8Z\nlYHsFWUkjJxpaPYYzth1BPoxbYdTcf2alyoCsphDuKySMkClDSFWqAZVXJmdAQ2H\nwEUvgLt7cpAYCPXas+9VOceTIG8XAZu53goML52l7ueHqY435ZCOeZOLFqmNUEGY\n9zV1K/4jJDyo0K5/K6xzcgRgs1KyWjT/2lJAsEG7j7P1r1KtYpjZy1860JB9nM8w\nYP0O6sKlHod2jCQObAznrKRNnYUUGYlkdXp/M6MejSPrQtoua/NtSn5h9FBHymvw\nGBYtljx3srsQTO6pwxE653h/4QouIZ3Kf7Twmw6CLS53ZsleplpBcZ4zm6K68Do/\n8ZRvfnIr1hk980LmIzf4aevALx/eCVB4HzspzOVq8CExCVg5BXdV4j9BSHmH6rV8\nPYAMdxXLzyt9uWMOVgQjJnWg4q6H8RcjJ/1NkAGoCOgFuaJu0f/jSyokfHr3Crio\nFZ7zpZ67cs7Dyz4ZfsEDV1om7WjsiQzR4tZSp6swXXMCAwEAAaOCAQQwggEAMA8G\nA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMB0GA1UdDgQWBBS8PeSkx7yL\nMQtV4sMHoqL429e7cDAfBgNVHSMEGDAWgBTGON+k2onDjBtKS2UHJa+z1LZHgjCB\nhAYDVR0fBH0wezA+oDygOoY4aHR0cDovL3BraS1jcmwuaXRuLmZ0Z3JvdXAvY3Js\nL29yYW5nZWludGVybmFsZzItcm9vdC5jcmwwOaA3oDWGM2h0dHA6Ly9wa2kub3Jh\nbmdlLmNvbS9jcmwvb3JhbmdlaW50ZXJuYWxnMi1yb290LmNybDAWBgNVHSAEDzAN\nMAsGCSqBegEQDAoBATANBgkqhkiG9w0BAQsFAAOCAgEAQzrAaBHlbm8sFTNaLCm9\n0imjsDrpCriwTAV3sxaFMXxuoQOIN7frc+tulEq1ATZNicbiRqHJq9BnIc1MGu/A\nhzxvFMBAjBnj8/wqAY0GsFr52axYQHwgANs9OIuSw8oz3/z1/6T/42JtK+WlIS/r\nmalTSJqa2CEe/fRVQJp5YcO0U1mIVHatcMq7Z9YVY6K8A7hb7qSs7GO36U7hStFt\n72s5++fdG+s5IDi2l6CMTZmelbhjsxJL/A9u8yrFJxniAHs5+ikwO+MoL2ZBC+0A\nJt8Xtjye32//LGAyvai1jJ4U501wjBvvdbul1xVARV/sHrYSE+hvDEtfHcSn92H4\nzC9C2ANMGGxykbDLWbVfqmGW5Iz4UWCEv6n451QckMJNJI7743qeUc0AHdMtPDtx\n+ItAxpaUIXINR9Lqo3erWefYLSne7vi86QdqCvwHWpzjuIJ9DXOT5qRCN0M859AA\nZFRBQn5zjPO1tyvtwb9/0o1AjL5NAMc88qQAmxkiC9/QzDL7LYxjKgkwCWx+DfJF\n/2Xe9zb3zGvT6xOjIELMQhNwnU37EMe4hSnn4dJBf0yNq4g3Rt3MmFFCDy6YE3PG\nrtfQ/CUwLPJb9Bqroqe4ryYPsVKpKe+71fEOKS8iHlFSggGZucZ63WKSyiBS1sJt\nV5qME0d/UK4/+4FTP7FcXmc=\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIFsDCCA5igAwIBAgIBADANBgkqhkiG9w0BAQ0FADBdMQswCQYDVQQGEwJGUjEP\nMA0GA1UECgwGT3JhbmdlMRgwFgYDVQQLDA9GUiA4OSAzODAxMjk4NjYxIzAhBgNV\nBAMMGk9yYW5nZSBJbnRlcm5hbCBHMiBSb290IENBMB4XDTE2MDUwMjExMzgwN1oX\nDTQxMDUwMjExMzgwN1owXTELMAkGA1UEBhMCRlIxDzANBgNVBAoMBk9yYW5nZTEY\nMBYGA1UECwwPRlIgODkgMzgwMTI5ODY2MSMwIQYDVQQDDBpPcmFuZ2UgSW50ZXJu\nYWwgRzIgUm9vdCBDQTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAOCL\n6xok2VzVyHsl9RucKjtEALfI1Cx9uu1AbflfsVP4YEpOHtDW4yfr39uSB9NvjVRJ\n8LPteLbsuCHaaDM6D4urluhjS73C2vorMEElgCCG1sjHa0pXDLVG7H9CegqlyZtn\nvbzquJbYsCZaywBYx4pVoe0JvPJKI7/nvE2IdI0Ue9Pr4v0nUiJhzZG1S04mDw4E\nMlTvW60lFLqnqXDA0itIJfpAwbAFqw1xsvFxXBnC/vUUbQctOi7je8YC/p5w0aoJ\no2wGkgDw2bGpO1VaeyfxTtyf41RUC2wuV9bO2vhV1aWf0vp59zybMlL/pMTmiaoa\nrVQr8aE/RGujn/mMqzT4JVmiCmD5bPy2OVKJnch7L/RNI/LropF17lPzpXboqk8M\n2K2+T7AxIJ0voanaxPXdZFElhxCg/XW04OaVrN4dO1x3njjR2MNizFhe79NZ7LEd\nefltcg11IXC6dKtCgs0/GLu6I6nYuSLRq6N5nwAs017IJw1RSFT2SYS6g6JjiAT+\nmr3dqXBSsNwRBug8oPxm82VNUHNcj/LqNYr8EEDqICd8PtRbEA6pSXMzxqO2zcRE\nDprWOUpL47Y5CKpcG5R+stezHgXM8ArsmT2A05+CQ3vey6pp/10q80gZ11iJDIqv\nGVVQvcvBqQ+0Eo2Rr5Kv9lPbzIvshxr6gSeEHo8nAgMBAAGjezB5MA8GA1UdEwEB\n/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMB0GA1UdDgQWBBTGON+k2onDjBtKS2UH\nJa+z1LZHgjAfBgNVHSMEGDAWgBTGON+k2onDjBtKS2UHJa+z1LZHgjAWBgNVHSAE\nDzANMAsGCSqBegEQDAoBATANBgkqhkiG9w0BAQ0FAAOCAgEAHm5FZlDv+2pzV7f4\nhfEDYWwFn2Lbqyn/Y8BsiyfvTX11AYOw41OeFLrk3FFM6+iLb14DXQcTwHjBux+2\nhtH4T3jyPeBmsnRzuxsb61TqYO1TnId8zzroyPTT+MHABmETyUf9uuSispNgJJXk\nYYxpVPVLUZNtSuoDuVkjHJOQ0NRTB+1XzfShuSwI4vtttlhRg6qVvgjnyP2tR8BY\nwkHWI+iHM8VUwoZYIyk3zRbFTdqWEyebUAm2aJ7LguroMTZ71ZxvD/LJfHybj8Hb\nrm2Tk+Dq5gvBiggfgs23lx2uWBJbMa8F7I7JH/+YsvwXmlQSkDPDcUopKestiKVb\nfAhY1NRDqMlwkOLLrGxPjAKCDYwVkxz0pkLnWDiDRNpLlDpJ6x3+dOX34vi+K3dq\nwl73JXdXo4NEXswVsVM8fD+cdHluS74pemMf/7olUfd60YYC+6RwQRyP/4I0WaNs\n6D4B8R3rc/X1obgvx6NfF55mFEwW3yhxRLjux5sYEBfkT8tTllVcRUwJCQumLUS9\nrnrCMsDkBfI/ZN+ITBcWi6cAyOcsIOk4up9aUvNJTMKUlEybDNN8cDp9+vLonN2h\nYMg1IfJ+IZs6x3rDr5MXuWTks34hEJidpmOTmOyy/QPHCK0BGRmUoYJr4CHS4FL2\nBtsnCPyUUO4gppjywfYrotQ2gTQ=\n-----END CERTIFICATE-----",
				"az_list": "eu-west-0a,eu-west-0b,eu-west-0c",
				"sg_default": "default",
				"sg_api": "EDGE_SG_SOCLE_K8S_API",
				"sg_etcd": "EDGE_SG_SOCLE_K8S_ETCD",
				"sg_worker": "EDGE_SG_SOCLE_K8S_WORKER",
				"cloud": "fe",
				"key_pair": "KeyPair-customer-edge",
				"internal_network_name": "2988804f-b329-4f4d-89bd-7b9132ad311e",
				"external_network_name": "admin_external_net",
				"internal_subnet_id": "59173644-ba53-4f8c-9a58-954453ca3629",
				"external_subnet_id": "f2da9b91-3cc1-4dde-a5f7-a603aa65a2c1",
				"loadbalancer_api_eip": "90.84.247.190",
				"loadbalancer_worker_eip": "90.84.178.199",
				"use_openstack_cinder_csi":"true"
			},
			"rancher" : {
				"RANCHER_URL": "https://rancher.edge.inter-cdnrd.orange-business.com/v3",
				"RANCHER_ACCESS_KEY": "token-pf82h",
				"RANCHER_SECRET_KEY": "kvhn56m5dtv2gfh5dgxmv7rjnpqfk8fp98zb6qknwl2rrf62p4sghc"
			},
			"s3" : {
				"S3_SERVICE_ENDPOINT": "oss.eu-west-0.prod-cloud-ocb.orange-business.com",
				"S3_BUCKET_NAME": "edge-bucket",
				"S3_ACCESS_KEY": "VTSQCXTEH0INKKWJ2IVV",
				"S3_SECRET_KEY": "eDPu3hmA82cnNY1iiFLp89rpfqHpcKBBVm5p1zH6",
				"S3_REGION": "eu-west-0"
			}
		}
	}
}
